FROM registry.gitlab.com/instantgame.online/images/steamcmd:latest AS install

USER root

RUN mkdir /opt/cs-go; chown steam:steam /opt/cs-go

USER steam

RUN steamcmd +login anonymous +force_install_dir /opt/cs-go +app_update 740 validate +quit

FROM registry.gitlab.com/instantgame.online/images/source-engine:latest

COPY --chown=steam --from=install /opt/cs-go /opt/cs-go

ENV LD_LIBRARY_PATH=/opt/cs-go:/opt/cs-go/bin

WORKDIR "/opt/cs-go"

ENTRYPOINT ["/opt/cs-go/srcds_linux"]

CMD ["-game", "csgo", "+map", "de_dust2", "-port", "27015"]
